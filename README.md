# Saturn

Big, but less dense, hackabe code editor. Reconstructed from Atom fragments

### Installing and setting things up...

##### Make sure that you have installed in your machine 
- Clojure
- nodeJS
- [shadow-cljs](https://shadow-cljs.github.io/docs/UsersGuide.html#_installation)

##### Compiling
```bash
$ shadow-cljs watch view
$ node out/view.js
```

