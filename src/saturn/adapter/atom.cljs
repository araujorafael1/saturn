(ns saturn.adapter.atom
  (:require [re-frame.core :as re]))

(defn- generate-spy
  ([] (generate-spy {}))
  ([passthrough]
   (let [calls (atom {})
         target {:calls calls :passthrough passthrough}
         spy #js {:get (fn [target prop receiver]
                         (if (contains? passthrough (keyword prop))
                           (get passthrough (keyword prop))
                           (let [{:keys [spies return]} (generate-spy)]
                             (swap! (:calls target) assoc (keyword prop) spies)
                             (constantly return))))}]
     {:spies calls
      :return (js/Proxy. target spy)})))

(defn- open-things [uri options]
  (re/dispatch [:open/uri {:uri uri :options options}]))

(def ^:private workspace (generate-spy {:open open-things}))
(def ^:private config (generate-spy))
(def ^:private notifications (generate-spy))
(def ^:private commands (generate-spy))

;; Unfortunately, Atom expects both an exports and a global var with different values...
(set! js/global.atom
  (clj->js {:workspace (:return workspace)
            :config (:return config)
            :project {:getPaths (constantly #js ["."])
                      :getRepositories (constantly #js [])
                      :relativizePath (fn [e] e)
                      :relative (fn [e] e)
                      :removePath (constantly nil)
                      :onDidChangePaths (constantly #js {:dispose #()})}
            :notifications (:return notifications)
            :commands (:return commands)}))
