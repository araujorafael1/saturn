(ns saturn.events
  (:require [re-frame.core :as re]
            [malli.core :as m]))

(defn- passthrough [event-name]
  (fn [{:keys [db]} [_ params]]
    {:db db event-name params}))

(defn- open-uri [{:keys [db]} [_ {:keys [uri] :as params}]]
  (if (string? uri)
    {:db db :open/file params}))
  ; (prn :URI uri))
  ; (if (string? uri)
  ;   (let [[_ protocol element] (re-matches #"^(.*?)://(.*)" uri)]
  ;     (if protocol
  ;       ))
  ;   (prn :OPEN (type uri))))
  ; (prn :OPEN! uri))

(defn register-events! []
  (re/reg-event-fx :open/uri open-uri))
  ; (re/reg-fx :uri/open uri-open))
