(ns saturn.view
  (:require [re-frame.core :as re]
            [promesa.core :as p]

            [reagent.core :as r]
            [reagent.dom :as r-dom]
            ["react" :as react]

            [saturn.adapter.atom]
            [saturn.events :as events]

            ["@monaco-editor/loader$default" :as loader]

            ["../js-deps/panel/pane" :as Pane]
            ["../js-deps/panel/dock" :as Dock]

            ["../atom/tree-view/lib/main.js" :as tree-view]
            ["fs" :as fs]
            ["path" :as path]))

(defn- async-monaco! [elem params]
  (p/let [^js monaco (.init loader)]
    (.. monaco
        -editor
        (create elem
                (clj->js params)))))

(defn create-dock! [item params]
  (let [dock (new Dock (clj->js (assoc params
                                       :viewRegistry {:getView (constantly item)})))]
    (set! (.. dock getElement -style -minWidth) "2px")
    (when-let [size (:size params)] (.. dock (setState #js {:size size})))
    (when (:hidable? params)
      (set! (.. dock getElement -onmouseenter)
        #(.. dock getElement (querySelector ".atom-dock-toggle-button")
             -classList (add "atom-dock-toggle-button-visible")))
      (set! (.. dock getElement -onmouseleave)
        #(.. dock getElement (querySelector ".atom-dock-toggle-button")
             -classList (remove "atom-dock-toggle-button-visible"))))
    (when-let [visible (:visible params)]
      (.show dock))
    dock))

(def left-dock
  (do
    (.activate tree-view)
    (let [d (create-dock! (doto (.. tree-view getTreeViewInstance -element)
                                (.. -style (setProperty "flex-grow" "1")))
                          {:location "left"
                           :size 245
                           :visible true
                           :hidable? true
                           :didActivate (constantly nil)})]
      d)))

(defn- html->react [elem props html]
  (let [ref (. react useRef nil)]
    (. react useEffect
      (fn [] (when-let [e (.-current ref)] (.appendChild e html)))
      #js [(.-current ref)])
    (. react createElement elem (-> props (assoc :ref ref) clj->js))))

(defn- function->react [elem props function]
  (let [[fun fun-set!] (.useState react)
        ref (.useRef react nil)]
    (. react useEffect (fn []
                         (when-let [e (and (not fun) (.-current ref))]
                           (p/let [res (function e)] (fun-set! res))))
                #js [(.-current ref)])
    (. react createElement elem (-> props (assoc :ref ref) clj->js))))

(def fs-promise (.-promises fs))
(declare editor)

(defn- open-in-monaco! [uri]
  (p/let [^js mon (.init loader)
          ; languages (.. mon -languages getLanguages)
          ; ext-name (.extname path uri)
          ; language (->> languages
          ;               (filter (fn [^js l] (->> l .-extensions (some #(= ext-name %)))))
          ;               first)
          text (. fs-promise readFile uri)
          text (str text)
          uri (.. mon -Uri (file uri))
          model (or (.. mon -editor (getModel uri))
                    (.. mon -editor (createModel text nil uri)))]
    (doto ^js editor
          (.setValue text)
          (.setModel model))))

(defn register-events! []
  (re/reg-fx :open/file (fn [{:keys [uri options]}]
                          (open-in-monaco! uri))))
                          ; (prn :WILL-OPEN uri options))))

(defn ^:dev/after-load main []
  (.config loader (clj->js {:paths {:vs "./node_modules/monaco-editor/dev/vs"}}))
  (events/register-events!)
  (register-events!)
  (let [elem (.getElement left-dock)]
    (r-dom/render
     [:atom-workspace-axis {:class "horizontal"}
      [:f> #(html->react "atom-panel-container"
                         {:class (.-className elem)}
                         elem)]
      [:atom-workspace-axis {:class "vertical"}
       [:atom-pane-container {:class "pane"}
        [:f> #(function->react "atom-pane" {:class "pane active"}
                               (fn [e]
                                 (p/let [e (async-monaco! e {:value "function a() { return 10; }"
                                                             :automaticLayout true
                                                             :language "javascript"
                                                             :minimap {:enabled false}})]
                                   (def editor e)
                                   e)))]]]]
     ; [: {:class "vertical"}]
     ; [:f> #(monaco-element {})]]
     ; [:f> #(html->react "atom-panel-container" {:class (.-className elem)} elem)]]]
     (js/document.querySelector "atom-workspace"))))
